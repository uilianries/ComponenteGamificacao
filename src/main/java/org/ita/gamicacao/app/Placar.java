package org.ita.gamicacao.app;

import org.ita.gamicacao.app.modules.Armazenamento;
import org.ita.gamicacao.app.modules.ArmazenamentoImpl;

import java.io.IOException;
import java.util.Map;

public class Placar {
    /**
     * ArmazenamentoImpl de jogaores
     */
    private Armazenamento armazenamento = new ArmazenamentoImpl();

    void setArmazenamento(final Armazenamento armazenamento) {
        this.armazenamento = armazenamento;
    }

    /**
     * Registrar pontos
     * @param nome nome do jogador
     * @param tipo tipo de pontos
     * @param quantidade quantidade de pontos
     * @throws IOException
     */
    public void registrarPonto(final String nome, final String tipo, final Integer quantidade) throws IOException {
        this.armazenamento.armazenar(nome, tipo, quantidade);
    }

    /**
     * Recupera pontos de um jogador
     * @param nome Nome do jogador a ser recuperado
     * @throws IOException
     * @return Mapa de tipo por quantidade de pontos
     */
    public Map<String, Integer> recuperarPontos(final String nome) throws IOException {
        return this.armazenamento.tabelaJogador(nome);
    }

    /**
     * Recupera ranking de jogadores
     * @param tipo tipo de pontos
     * @return Ranking do jogadores
     */
    public Map<Integer, String> rankingJogador(final String tipo) {
        return this.armazenamento.rankingJogador(tipo);
    }
}
