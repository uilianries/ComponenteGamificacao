package org.ita.gamicacao.app.modules;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by uilian on 9/18/16.
 */
public class ArmazenamentoMock implements Armazenamento {
    /**
     * Armazena um novo jogador com sau pontuação
     * @param nome Nome do jagador
     * @param ponto Tipo do ponto
     * @param quantidade Quantidade de pontos
     */
    public void armazenar(final String nome, final String ponto, final Integer quantidade) throws IOException {
    }

    /**
     * Recupera jogador da lista
     * @param jogador Jogador que deve ser encontrado na lista
     * @return Instancia encontrada
     */
    private Jogador getJogador(final Jogador jogador) throws IndexOutOfBoundsException {
        return new Jogador("Seu Madruga");
    }

    /**
     * Recuperar quantos pontos de um tipo tem um usuário
     * @param nome Nome do jogador
     * @param tipo Tipo do ponto para ser regatado
     * @return Quantidade de pontos
     * @throws IndexOutOfBoundsException Se nome do jogador não constar na tabela
     */
    public Integer pontosPorTipo(final String nome, final String tipo) throws IndexOutOfBoundsException {
        return 10;
    }

    /**
     * Recupera todos os jogadores
     * @return Lista com todos os jogadores
     */
    public Set<String> jogadoresComPontos() {
        Set<String> jogadores = new HashSet<>();
        jogadores.add("Seu Madruga");
        jogadores.add("Girafales");
        return jogadores;
    }

    /**
     * Recupera os tipos de pontos registrados com cada jogador
     * @return Lista dos tipos de pontos
     */
    public Set<String> pontosDeJogadores() {
        Set<String> pontos = new HashSet<>();
        pontos.add("xinforinfola");
        return pontos;
    }

    /**
     * Recupera relação de tipo de pontos por quantidade
     * @param nome nome do jogador alvo
     * @return Pontos do jogador
     */
    public Map<String, Integer> tabelaJogador(final String nome) {
        Map<String, Integer> tabela = new HashMap<>();
        tabela.put("xinforinfola", 30);
        tabela.put("bola quadrada", 10);
        return tabela;
    }

    /**
     * Recupera ranking de jogadores
     * @param tipo tipo de pontos
     * @return Ranking do jogadores
     */
    public Map<Integer, String> rankingJogador(final String tipo) {
        Map<Integer, String> ranking = new HashMap<>();
        ranking.put(10, "Seu Madruga");
        ranking.put(5, "Quico");
        ranking.put(1, "Chiquinha");
        return ranking;
    }
}
