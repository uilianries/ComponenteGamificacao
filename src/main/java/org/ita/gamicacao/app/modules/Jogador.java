package org.ita.gamicacao.app.modules;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Representação de um jogador
 */
public class Jogador implements Serializable {
    private Map<String, Integer> pontos = new HashMap<String, Integer>();
    private String nome;

    /**
     * Recupera nome do jogador
     */
    public final String getNome() {
        return this.nome;
    }

    /**
     * Getter para pontos
     * @return pontuação do jogador
     */
    public final Map<String, Integer> getPontos() {
        return this.pontos;
    }
    /**
     * Cria um novo jogador
     * @param nome Nome do jogador
     */
    public Jogador(String nome) {
        this.nome = nome;
    }
    /**
     * Adiciona pontução para um tipo.
     * Se o tipo já existir, então a pontuação será incrementada
     * @param tipo Tipo de pontos Ex. estrela
     * @param valor Valor da pontuação
     */
    public void adicionarPontos(String tipo, Integer valor) {
        if (pontos.containsKey(tipo)) {
            valor += pontos.get(tipo);
        }
        pontos.put(tipo, valor);
    }

    /**
     * Remove pontução para um tipo.
     * Se o tipo já existir, então a pontuação será decrementada
     * @param tipo Tipo de pontos Ex. estrela
     * @param valor Valor da pontuação
     */
    public void removerPontos(String tipo, Integer valor) {
        if (pontos.containsKey(tipo)) {
            valor = pontos.get(tipo) - valor;
        }
        pontos.put(tipo, valor);
    }

    /**
     * Retorna a pontuação de determinada pontuação
     * @param tipo Tipo dos pontos filtrado
     * @IndexOutOfBoundsException
     * @return Valor da pontuação encontrada
     */
    public Integer getPontuacao(String tipo) throws IndexOutOfBoundsException {
        Integer valor = pontos.get(tipo);
        if (valor == null) {
            throw new IndexOutOfBoundsException("Tipo não inserido na tabela");
        }
        return valor;
    }

    @Override
    public int hashCode() {
        return this.nome.hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        return this.nome.equals(((Jogador) obj).nome);
    }
}
