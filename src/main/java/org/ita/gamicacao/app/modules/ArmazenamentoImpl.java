package org.ita.gamicacao.app.modules;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Guarda e recupera as informações de um arquivo
 */
public class ArmazenamentoImpl implements Armazenamento {
    /**
     * Jogares do jogo
     */
    private HashSet<Jogador> jogadores = new HashSet<Jogador>();
    /**
     * Caminho da base de dados
     */
    private Path arquivo;

    /**
     * Instancia objete de ArmazenamentoImpl
     */
    public ArmazenamentoImpl() {
        String tempdir = System.getProperty("java.io.tmpdir");
        arquivo = Paths.get(tempdir, "componente_gamificado");
        try {
            carregar();
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }
    }

    /**
     * Armazenar em disco o time de jogadores
     */
    private void salvar() throws IOException {
        FileOutputStream fos = new FileOutputStream(arquivo.toString());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(jogadores);
        oos.close();
    }

    /**
     * Carregar time de jogadores do disco
     */
    private void carregar() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(arquivo.toString());
        ObjectInputStream ois = new ObjectInputStream(fis);
        this.jogadores = (HashSet<Jogador>) ois.readObject();
        ois.close();
    }

    /**
     * Armazena um novo jogador com sau pontuação
     * @param nome Nome do jagador
     * @param ponto Tipo do ponto
     * @param quantidade Quantidade de pontos
     */
    public void armazenar(final String nome, final String ponto, final Integer quantidade) throws IOException {
        Jogador jogador = new Jogador(nome);
        try {
            jogador = getJogador(jogador);
        } catch (IndexOutOfBoundsException except) {
            System.out.println(except.getMessage());
        }
        jogador.adicionarPontos(ponto, quantidade);
        jogadores.add(jogador);
        salvar();
    }

    /**
     * Recupera jogador da lista
     * @param jogador Jogador que deve ser encontrado na lista
     * @return Instancia encontrada
     */
    private Jogador getJogador(final Jogador jogador) throws IndexOutOfBoundsException {
        if (!this.jogadores.contains(jogador)) {
            throw new IndexOutOfBoundsException("");
        }
        Jogador result = new Jogador("");
        for (Jogador cache : jogadores) {
            if (cache.equals(jogador)) {
                result = cache;
                break;
            }
        }
        return result;
    }

    /**
     * Recuperar quantos pontos de um tipo tem um usuário
     * @param nome Nome do jogador
     * @param tipo Tipo do ponto para ser regatado
     * @return Quantidade de pontos
     * @throws IndexOutOfBoundsException Se nome do jogador não constar na tabela
     */
    public Integer pontosPorTipo(final String nome, final String tipo) throws IndexOutOfBoundsException {
        Jogador cache = new Jogador(nome);
        cache = getJogador(cache);
        return cache.getPontuacao(tipo);
    }

    /**
     * Recupera todos os jogadores
     * @return Lista com todos os jogadores
     */
    public Set<String> jogadoresComPontos() {
        Set<String> nomes = new HashSet<>();
        for (Jogador jogador : jogadores) {
            nomes.add(jogador.getNome());
        }
        return nomes;
    }

    /**
     * Recupera os tipos de pontos registrados com cada jogador
     * @return Lista dos tipos de pontos
     */
    public Set<String> pontosDeJogadores() {
        Set<String> pontos = new HashSet<>();
        for (final Jogador jogador : jogadores) {
            for (Map.Entry<String, Integer> it : jogador.getPontos().entrySet()) {
                pontos.add(it.getKey());
            }
        }
        return pontos;
    }

    /**
     * Recupera relação de tipo de pontos por quantidade
     * @param nome nome do jogador alvo
     * @return Pontos do jogador
     */
    public Map<String, Integer> tabelaJogador(final String nome) {
        Jogador jogador = new Jogador(nome);
        jogador = getJogador(jogador);
        Map<String, Integer> placar = new HashMap<>();
        for (final Map.Entry<String, Integer> it: jogador.getPontos().entrySet()) {
            if (it.getValue() > 0) {
                placar.put(it.getKey(), it.getValue());
            }
        }
        return placar;
    }

    /**
     * Recupera ranking de jogadores
     * @param tipo tipo de pontos
     * @return Ranking do jogadores
     */
    public Map<Integer, String> rankingJogador(final String tipo) {
        Set<String> jogadores = jogadoresComPontos();
        Map<Integer, String> ranking = new HashMap<>();
        for (final String nome : jogadores) {
            Map<String, Integer> tabela = tabelaJogador(nome);
            for (Map.Entry<String, Integer> it : tabela.entrySet()) {
                if (it.getKey().equals(tipo)) {
                    ranking.put(it.getValue(), nome);
                }
            }
        }
        return ranking;
    }
}
