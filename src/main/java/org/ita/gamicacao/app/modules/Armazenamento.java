package org.ita.gamicacao.app.modules;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by uilian on 9/18/16.
 */
public interface Armazenamento {
    /**
     * Armazena um novo jogador com sau pontuação
     * @param nome Nome do jagador
     * @param ponto Tipo do ponto
     * @param quantidade Quantidade de pontos
     */
    public void armazenar(final String nome, final String ponto, final Integer quantidade) throws IOException;

    /**
     * Recuperar quantos pontos de um tipo tem um usuário
     * @param nome Nome do jogador
     * @param tipo Tipo do ponto para ser regatado
     * @return Quantidade de pontos
     * @throws IndexOutOfBoundsException Se nome do jogador não constar na tabela
     */
    public Integer pontosPorTipo(final String nome, final String tipo);

    /**
     * Recupera todos os jogadores
     * @return Lista com todos os jogadores
     */
    public Set<String> jogadoresComPontos();

    /**
     * Recupera os tipos de pontos registrados com cada jogador
     * @return Lista dos tipos de pontos
     */
    public Set<String> pontosDeJogadores();

    /**
     * Recupera relação de tipo de pontos por quantidade
     * @param nome nome do jogador alvo
     * @return Pontos do jogador
     */
    public Map<String, Integer> tabelaJogador(final String nome);

    /**
     * Recupera ranking de jogadores
     * @param tipo tipo de pontos
     * @return Ranking do jogadores
     */
    public Map<Integer, String> rankingJogador(final String tipo);
}
