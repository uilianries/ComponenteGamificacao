package org.ita.gamicacao.app;

import org.ita.gamicacao.app.modules.ArmazenamentoMock;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by uilian on 9/18/16.
 */
public class PlacarTest {
    private Placar placar;

    @Before
    public void setUp() {
        this.placar = new Placar();
        this.placar.setArmazenamento(new ArmazenamentoMock());
    }

    @Test
    public void registrarPontos() throws IOException {
        this.placar.registrarPonto("Seu Madruga", "foobar", 10);
    }

    @Test
    public void pontosPorTipo() throws IndexOutOfBoundsException, IOException {
        Map<String, Integer> tabela = new HashMap<>();
        tabela.put("xinforinfola", 30);
        tabela.put("bola quadrada", 10);
        Map<String, Integer> pontos = this.placar.recuperarPontos("Seu Madruga");
        assertEquals(tabela, pontos);
    }

    @Test
    public void rankingJogador() {
        Map<Integer, String> ranking = new HashMap<>();
        ranking.put(10, "Seu Madruga");
        ranking.put(5, "Quico");
        ranking.put(1, "Chiquinha");
        Map<Integer, String>  placar = this.placar.rankingJogador("estrela");
        assertEquals(ranking, placar);
    }

}