package org.ita.gamicacao.app.modules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Valida comportamento do Jogador
 */
public class JogadorTest {
    private Jogador jogador;
    private final String tipoPonto = "estrela";
    private final Integer quantidade = 10;
    private final String nome = "Seu Madruga";


    @org.junit.Before
    public void setUp() throws Exception {
        jogador = new Jogador(nome);
    }

    @Test
    public void insereEstrela() {
        this.jogador.adicionarPontos(tipoPonto, quantidade);
        assertEquals(quantidade, this.jogador.getPontuacao(tipoPonto));
    }

    @Test
    public void removeEstrela() {
        final Integer remocao = quantidade / 2;
        this.jogador.adicionarPontos(tipoPonto, quantidade);
        assertEquals(quantidade, this.jogador.getPontuacao(tipoPonto));
        this.jogador.removerPontos(tipoPonto, remocao);
        assertEquals(remocao, this.jogador.getPontuacao(tipoPonto));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void tipoInvalido() {
        this.jogador.getPontuacao(tipoPonto);
    }
}