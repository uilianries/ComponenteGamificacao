package org.ita.gamicacao.app.modules;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Valida comportamento do armazenamentoImpl
 */
public class ArmazenamentoTest {
    /**
     * Time para ser validado
     */
    private Armazenamento armazenamento;

    @Before
    public void setUp() throws IOException, ClassNotFoundException {
        this.armazenamento = new ArmazenamentoImpl();
    }

    @After
    public void tearDown() throws IOException {
        String tempdir = System.getProperty("java.io.tmpdir");
        Path arquivo = Paths.get(tempdir, "componente_gamificado");
        Files.delete(arquivo);
    }

        @Test
    public void armazenar() throws Exception {
        this.armazenamento.armazenar("Seu Madruga", "xinforinfola", 10);
    }

    @Test
    public void pontosPorTipo() throws IndexOutOfBoundsException, IOException {
        final String nome = "Quico";
        final String tipo = "xinforinfola";
        final Integer pontuacao = 10;

        this.armazenamento.armazenar(nome, tipo, pontuacao);
        assertEquals(pontuacao, this.armazenamento.pontosPorTipo(nome, tipo));
        this.armazenamento.armazenar(nome, tipo, pontuacao);
        assertEquals(new Integer(20), this.armazenamento.pontosPorTipo(nome, tipo));
    }

    @Test
    public void jogadoresComPontos() throws IOException {
        Set<String> nomes = new HashSet<String>();
        nomes.add("Seu Madruga");
        nomes.add("Quico");
        nomes.add("Jaiminho");
        for (String nome : nomes) {
            this.armazenamento.armazenar(nome, "foo", 10);
        }
        assertEquals(nomes, this.armazenamento.jogadoresComPontos());
    }

    @Test
    public void pontosDeJogadores() throws IOException {
        this.armazenamento.armazenar("Seu Madruga", "disco voador", 5);
        this.armazenamento.armazenar("Seu Barriga", "aluguel", 14);
        this.armazenamento.armazenar("Girafales", "charuto", 1);

        Set<String> pontos = this.armazenamento.pontosDeJogadores();
        assertTrue(pontos.contains("disco voador"));
        assertTrue(pontos.contains("aluguel"));
        assertTrue(pontos.contains("charuto"));
    }

    @Test
    public void rankingJogador() throws IOException {
        final String tipoPonto = "xinforinfola";
        Map<Integer, String> pontos = new HashMap<>();
        pontos.put(5, "Chaves");
        pontos.put(15, "Quico");
        pontos.put(10, "Seu Madruga");

        for (final Map.Entry<Integer, String> it : pontos.entrySet()) {
            this.armazenamento.armazenar(it.getValue(), tipoPonto, it.getKey());
        }
        Map<Integer, String> ranking = this.armazenamento.rankingJogador("xinforinfola");
        assertEquals(pontos, ranking);
    }

}